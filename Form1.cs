﻿using CefSharp;
using CefSharp.WinForms;
using Reactive.Framework.Error;
using ReactiveUI.Commands;
using ReactiveUI.Renderer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReactiveUI
{
    public partial class Form1 : Form
    {
        static CefInstance cefInstance = null;
        public ChromiumWebBrowser chromeBrowser = null;

        public Form1()
        {
            InitializeComponent();
            chromeBrowser = CefInstance.InitializeCEF(this);
            Application.ApplicationExit += Debug.Exit;

            cefInstance = new CefInstance(chromeBrowser, this);
            cefInstance.chromeBrowser.RegisterJsObject("commonCommands", new CommonCommands(chromeBrowser, this));
            cefInstance.chromeBrowser.RegisterJsObject("navigationCommands", new NavigationCommands(chromeBrowser, this));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ReactiveAI.Speech.SpeechTalk.Talk("Welcome Back!");
        }

        private void Form1_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            Cef.Shutdown();
        }
    }
}
