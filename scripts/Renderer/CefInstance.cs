﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharp;
using CefSharp.WinForms;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace ReactiveUI.Renderer
{
    class CefInstance
    {

        private static ChromiumWebBrowser _instanceBrowser = null;
        public ChromiumWebBrowser chromeBrowser { get { return _instanceBrowser; } }

        private static Form1 _instanceMainWindow = null;
        public static string prevPage;

        public CefInstance(ChromiumWebBrowser originalBrowser, Form1 form)
        {
            _instanceBrowser = originalBrowser;
            _instanceMainWindow = form;
        }

        public void showDevTools()
        {
            _instanceBrowser.ShowDevTools();
        }

        public static ChromiumWebBrowser InitializeCEF(Form1 form)
        {
            CefSettings settings = new CefSettings();
            form.Text = "REACTive";

            if (!File.Exists(Constants.htmlResource+"index.html"))
            {
                GUI.MessageBox.ShowMessage("Unable to load main interface", "ERROR");
                Application.Exit();
            }

            Cef.Initialize(settings);
            ChromiumWebBrowser chromeBrowser = new ChromiumWebBrowser(Constants.htmlResource + "index.html");
            form.Controls.Add(chromeBrowser);
            chromeBrowser.Dock = DockStyle.Fill;

            BrowserSettings browserSettings = new BrowserSettings();
            browserSettings.FileAccessFromFileUrls = CefState.Enabled;
            browserSettings.UniversalAccessFromFileUrls = CefState.Enabled;
            chromeBrowser.BrowserSettings = browserSettings;

            _instanceMainWindow = form;
            Reactive.Framework.Error.Debug.Log(string.Format("Initialized Renderer | CEF# v{0}", Cef.CefSharpVersion));
            return chromeBrowser;
        }

        public static void LoadPage(string page, ChromiumWebBrowser browser)
        {
            if (!File.Exists(Constants.htmlResource + page))
            {
                Reactive.Framework.Error.Debug.LogFatal("Renderer: Unable to render the requested page", true, "Unable to render the requested page");
            }

            char splitDelimiter = '/';
            string[] splitText = browser.Address.Split(splitDelimiter);
            prevPage = splitText[splitText.Length - 1];

            browser.Load(Constants.htmlResource + page);
        }

        public static void GoBack(ChromiumWebBrowser browser)
        {
            browser.Load(Constants.htmlResource + prevPage);
        }
    }

}
